import csv
from datetime import datetime
import tkinter as tk
from tkinter import filedialog

def returnNumberOfLines(csv_file):
    # Open the file in read mode
    with open(csv_file, 'r') as file:
        # Use a loop to iterate through each line and count the lines
        line_count = sum(1 for line in file)
    # Print the number of lines
    return line_count

# Create a Tkinter root window
root = tk.Tk()
root.withdraw()  # Hide the root window

# Prompt the user to select a file
file_path = filedialog.askopenfilename(
    initialdir="./",  # Set the initial directory to the current folder
    title="Select a CSV file",
    filetypes=(("CSV files", "*.csv"), ("All files", "*.*"))
)

# Pfad zur CSV-Datei
csv_file = file_path#"Abfuhrkalender-Huenfeld-2023.csv"

# Neuer Dateiname für die modifizierte CSV-Datei
output_file = "calendar.h"

# Ersetzungen definieren
replacements = {
    "Restmüll grün 4-wöchentlich": "1",
    "Bio-Tonne": "2",
    "Gelbe Tonne": "3",
    "Papier-Tonne": "4"
}

# Active time value in seconds
alertTime = 18*3600-86400
activeTime = 4*3600

# Lines to add at the beginning and end of the exported CSV
line_count=(returnNumberOfLines(csv_file)-1)*2
header_lines = [
    "#define EVENT_CT "+str(line_count),
    "",
    "int events[EVENT_CT][2] = {",
]
footer_lines = [
    "};"
]

# CSV-Datei öffnen und Daten einlesen
with open(csv_file, "r") as file, open(output_file, "w", newline="", encoding="utf-8") as output:
    csv_reader = csv.reader(file)
    csv_writer = csv.writer(output)
    
    # Write the header lines at the beginning of the file
    for line in header_lines:
        output.write(line + "\n")

    # Datenzeilen durchgehen und nur die letzten zwei Spalten schreiben
    # Ersten Eintrag überspringen
    next(csv_reader)
    data = []
    for row in csv_reader:
        last_two_columns = row[-2:]
    
        for word, initial in replacements.items():
            last_two_columns[0] = last_two_columns[0].replace(word, initial)

        # Letzte Spalte bearbeiten: Erste 3 Zeichen löschen und in UNIX-Zeit umwandeln
        last_column = last_two_columns[-1]
        last_two_columns[-1] = int(datetime.strptime(last_column[3:], "%d.%m.%Y").timestamp()) + alertTime

        data.append(last_two_columns)

    # Reorder the data list based on the value in the last column in ascending order
    sorted_data = sorted(data, key=lambda x: x[-1])

    # Write the sorted data to the output file
    for row in sorted_data:
        last_two_columns = row

        # Write the original row
        last_two_columns[0] = "{" + str(last_two_columns[0])
        last_two_columns[-1] = str(last_two_columns[-1]) + "}"
        last_two_columns[0] = "\t" + last_two_columns[0]
        csv_writer.writerow(last_two_columns)

        # Copy the row, insert it after itself, and update the last column with activeTime
        copied_row = last_two_columns[:]
        copied_row[0] = "\t{0"
        copied_row[-1] = copied_row[-1].replace("}", "")
        copied_row[-1] = int(copied_row[-1]) + activeTime
        copied_row[-1] = str(copied_row[-1]) + "}"
        csv_writer.writerow(copied_row)

    # Write the footer lines at the end of the file
    for line in footer_lines:
        output.write(line + "\n")

print("Die Daten wurden erfolgreich in 'calendar.h' geschrieben.")
