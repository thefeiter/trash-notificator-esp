#include <Arduino.h>

/*
    HTTP over TLS (HTTPS) example sketch
    This example demonstrates how to use
    WiFiClientSecure class to access HTTPS API.
    We fetch and display the status of
    esp8266/Arduino project continuous integration
    build.
    Created by Ivan Grokhotkov, 2015.
    This example is in public domain.
*/

#include <ESP8266WiFi.h>
#include "creds.h"
#include "FastLED.h"
#include "calendar.h"
#include <utility>

// Define Pin and the array of leds
#define DATA_PIN D8
#define NUM_LEDS 9
CRGB leds[NUM_LEDS];
const int switchPin = D4;

const char *ssid = STASSID;
const char *password = STAPSK;
time_t now = time(nullptr);
time_t onTime;
time_t offTime;
int event;
boolean resetLight;


void setColor(CRGB setColor)
{
  for(int i=0;i<NUM_LEDS;i++){
      leds[i] = setColor;
  }
}

void startUpBlinking()
{
  setColor(CRGB::Black);
  FastLED.show();
  FastLED.delay(1000);
  setColor(CRGB::Red);
  FastLED.show();
  FastLED.delay(1000);
  setColor(CRGB(255,115,0));
  FastLED.show();
  FastLED.delay(1000);
  setColor(CRGB::Green);
  FastLED.show();
  FastLED.delay(1000);
  setColor(CRGB::Black);
  FastLED.show();
  FastLED.delay(1000);
}



// int getEvent(time_t currentTime)
std::pair<int, int> getEvent(time_t currentTime)
{
  int previousEvent = 0;
  int doubleEvent = 0;
  time_t previousTime = 0;
  for (int i = 0; i < EVENT_CT; i++)
  {   
    if (currentTime < events[i][1]) // Wenn Event in der Liste in der Zukunft Liegt
    {
      if (events[i][1] == events[i+2][1])
      {
        Serial.println("Doppeltermin");
        Serial.print(events[i+1][0]);
        Serial.print(" mit Zeitstempel ");
        Serial.println(events[i+1][1]);
        doubleEvent = events[i+1][0]; 
      }
      Serial.print("CurrentEvent: "); //Letztes Event in der Liste, das noch nicht in der Zukunft liegt
      Serial.print(previousEvent);
      Serial.print(", EventTime: "); //Zeitstempel dieses letzten Events
      Serial.print(previousTime);
      Serial.print(", Current Time: "); // Aktueller Zeitstempel
      Serial.println(currentTime);
      return std::make_pair(previousEvent, doubleEvent);
    }
    previousEvent = events[i][0];
    previousTime = events[i][1];
  }
  return {-1,-1};
}  



void setup()
{
  pinMode(switchPin, INPUT_PULLUP);             // Set D4 as input with internal pull-up resistor
  Serial.begin(115200);                         // Set Serial baud rate
  
  Serial.print("Connecting to: ");              // Print Network connecting to
  Serial.println(ssid);

  WiFi.mode(WIFI_STA); // Setup WiFi connection
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) // Wait for connection
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println(); // Print connection Info
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());


    // Set time via NTP, as required for x.509 SSL validation
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");

  Serial.print("Waiting for NTP time sync: ");
  //Check ob Zeitsync funktioniert hat
  while (now < 8 * 3600 * 2)
  {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  Serial.println("");
  //Darstellung Zeitstempel
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.println(asctime(&timeinfo));


  //LED definition Typ und Array
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(220);

  startUpBlinking();
}



void loop() {
  
  // Read the state of the switch
  bool switchPressed = !digitalRead(switchPin);

  // Check the switch state and perform actions accordingly
  if (switchPressed && !resetLight) {
    // Switch is pressed
    Serial.println("Reset activated");
    resetLight = true;
  }


  now = time(nullptr);
  // int event = getEvent(now);
  std::pair<int, int> event = getEvent(now);

  if(resetLight && event.first !=0){
    event.first=-2;
    event.second=-2;
  }else{
    resetLight=false;
  }

  switch ( event.first )
  {
    case 0: //Kein Event
        setColor(CRGB::Black);
        break;
    case 1: //Schwarze Tonne
        setColor(CRGB::White);
        break;
    case 2: //Biotonne
        setColor(CRGB::Green);
        break;
    case 3: //Gelbe Tonne
        setColor(CRGB(255,115,0));
        break;
    case 4: //Blaue Tonne
        setColor(CRGB::Blue);
        break;
    case -1: //Fehler
        while(true)
        {
          setColor(CRGB::Red);
          FastLED.show();
          FastLED.delay(1000);
          setColor(CRGB::Black);
          FastLED.show();
          FastLED.delay(1000);
          delay(1);
        }
        break;    
    default:
        setColor(CRGB::Black);
  }

  FastLED.show();
  FastLED.delay(2000);

  switch ( event.second )
  {
    case 0: //Kein Event
        setColor(CRGB::Black);
        break;
    case 1: //Schwarze Tonne
        setColor(CRGB::White);
        break;
    case 2: //Biotonne
        setColor(CRGB::Green);
        break;
    case 3: //Gelbe Tonne
        setColor(CRGB(255,115,0));
        break;
    case 4: //Blaue Tonne
        setColor(CRGB::Blue);
        break;
    case -1: //Fehler
        while(true)
        {
          setColor(CRGB::Red);
          FastLED.show();
          FastLED.delay(1000);
          setColor(CRGB::Black);
          FastLED.show();
          FastLED.delay(1000);
          delay(1);
        }
        break;    
    default:
        setColor(CRGB::Black);
  }

  FastLED.show();
  FastLED.delay(2000);
}